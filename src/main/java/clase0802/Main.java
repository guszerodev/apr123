package clase0802;
// List +
// remover elementos
// agregar elementos

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class JLista<T> implements List<T>{

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}

class JListaInt implements List<Integer> {

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Integer get(int index) {
        return null;
    }

    @Override
    public Integer set(int index, Integer element) {
        return null;
    }

    @Override
    public void add(int index, Integer element) {

    }

    @Override
    public Integer remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }
}

class Lista {
    private int[] datos = new int[10];
    static int count = 0;

    // 0,1,2,3,4
    // 1,4,5,5,5,,
    //obtener
    //desplegar los elementos de la lista
    void remover(int indice) { //2
        for (int i = indice; i < count; i++) {
            datos[i] = datos[i + 1];
        }
        count--;
    }

    public void agregar(int dato) {
        if (count == datos.length) {
            int[] datos2 = new int[datos.length + 10];
            for (int i = 0; i < datos.length; i++) {
                datos2[i] = datos[i];
            }
            datos = datos2;
        }

        datos[count] = dato;
        count++;
    }
}

class Unos implements Iterator<Integer>{
    int contador = 3;
    public boolean hasNext() {
        return contador > 0;
    }
    public Integer next() {
        contador--;
        return 1;
    }
}
class Numeros implements Iterable<Integer> {
    public Iterator<Integer> iterator() {
        return new Unos();
    }
}

public class Main {
    public static void main(String[] args) {
        Lista lista = new Lista();
        lista.agregar(1);
        lista.agregar(2);

        Iterator<Integer> enteros = new Iterator<Integer>() {
            int contador = 1;
            public boolean hasNext() {
                return contador > 0;
            }
            public Integer next() {
                contador--;
                return 1;
            }
        };
        while (enteros.hasNext()){
            System.out.println(enteros.next());
        }

        System.out.println("---------Numeros-------------");
        Numeros numeros = new Numeros();
        for (int n : numeros) {
            System.out.println(n);
        }
        // Listas enlazadas, ver el concepto de Nodo

        //System.out.println(enteros.hasNext());
    }
}
