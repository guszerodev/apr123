package clase0810;

public interface Lista<T> {
    int longitud();
    boolean vacia();
    boolean contiene(T dato);
    boolean agregar(T dato);
    boolean remover(T dato);
    T obtener(int indice);
    //T establecer(int indice, T dato);
    void invertir();
}
