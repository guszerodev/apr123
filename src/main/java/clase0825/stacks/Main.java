package clase0825.stacks;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Stack<Character> tokens = getTokens("(1+(2)+3)");
        Stack<Character> tokens2 = getTokens("(1+(2)+3)");
        System.out.println(validate(tokens));
        System.out.println(validate(tokens2));
        //validate(tokens);
    }
    public static boolean validate(Stack<Character> tokens){
        Stack<Character> brackets = new Stack<>();
        Stack<Character> reverse = new Stack<>();
        while (tokens.size() > 0) {
            reverse.push(tokens.pop());
        }
        while (reverse.size() > 0) {
            Character c = reverse.pop();
            if(c == '(') {
                brackets.push(c);
            } else if (c == ')') {
                if (brackets.size() == 0) return false;
                brackets.pop();
            }
        }

        System.out.println(brackets);
        return brackets.size() == 0;
    }
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }

        return tokens;
    }
}
