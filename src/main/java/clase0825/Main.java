package clase0825;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Student implements Comparable<Student> {
    String name;
    int score;
    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }
    // by score
    // by name
    // by score and name
    public int compareTo(Student o) {
        //return score - o.score;
        //return Integer.compare(score, o.score);
        //return Integer.valueOf(score).compareTo(Integer.valueOf(o.score));
        return name.toLowerCase().compareTo(o.name.toLowerCase());
    }
    public String toString() {
        return "name:" + name + ", score: " + score;
    }
}

public class Main {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Std 1", 80));
        list.add(new Student("Std 4", 70));
        list.add(new Student("Std 3", 90));
        list.add(new Student("Std 1", 50));

        list.sort(Comparator.naturalOrder());
        for (Student s: list ) {
            System.out.println(s);
        }

    }
}

