package clase0816;
class Nodo<T> {
    T dato;
    Nodo<T> sig;
    public Nodo(T dato) { this.dato = dato;}
    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}
public class Main {
    public static void main(String[] args) {
        Nodo<Integer> n1 = new Nodo<>(1);
        Nodo<Integer> n2 = new Nodo<>(2);
        Nodo<Integer> n3 = new Nodo<>(3);
        Nodo<Integer> n4 = new Nodo<>(4);
        Nodo<Integer> n5 = new Nodo<>(5);
        n1.sig = n2;
        n2.sig = n3;
        n3.sig = n4;
        n4.sig = n5;
        System.out.println(n1);
    }
}
