package clase0805.refs;
class A {
    char info;
    B b;
}
class B {
    C c;
}
class C{

}
public class Main {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a);
        a.info = 'f';
        B b = new B();
        System.out.println(b);
        C c = new C();
        System.out.println(c);
        a.b = b;
        a.b.c = c;
    }
}
