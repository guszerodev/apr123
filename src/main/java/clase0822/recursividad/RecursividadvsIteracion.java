package clase0822.recursividad;

public class RecursividadvsIteracion {

    public static long factorialIter(long numero) {
        long factorial = 1;
        for (long i = numero; i > 1 ; i--) {
            factorial = factorial * i;
        }
        return factorial;
    }

    // Factorial: F(n) = n * F(n-1)
    public static long factorialRecu(long numero) {
        if (numero > 1) // i > 1
            // factorial = factorial * i
            // i--
            return numero * factorialRecu(numero - 1);

        return 1; // factorial = 1, (primer caso)
    }

    // Secuencia de Fibonacci
    // Y(n) = Y(n-1) + Y(n-2)
    public static long fibonacciRecu(long numero) {
        //caso base
        if (numero <= 1)
            return numero;
        // otros casos
        return fibonacciRecu(numero - 1) + fibonacciRecu(numero - 2);
    }

    public static long fibonacciIter(long numero) {
        if (numero <= 1)
            return numero;

        int n_1 = 0;
        int n_2 = 0;
        int actual = 1;

        for (int i = 1; i < numero; i++) {
            n_2 = n_1;
            n_1 = actual;
            actual = n_2 + n_1;
        }

        return actual;
    }
}

