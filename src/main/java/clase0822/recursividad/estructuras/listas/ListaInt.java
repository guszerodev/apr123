package clase0822.recursividad.estructuras.listas;

public class ListaInt {
    private int dato;
    private ListaInt sig;
    public void agregar(int dato) {
        if (sig == null) {
            this.dato = dato;
            sig = new ListaInt();
        } else {
            sig.agregar(dato);
        }
    }
    public void remover(int dato) {
        // need recursive implementation for an extra point
        // send MR with the solution and unit tests
    }
    public String toString() {
        return "{dato = " + dato + ", sig="+ sig  + "}";
    }
}
