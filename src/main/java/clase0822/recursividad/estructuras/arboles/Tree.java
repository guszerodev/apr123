package clase0822.recursividad.estructuras.arboles;

public class Tree {
    Tree der;
    Tree izq;
    int dato;

    public void add(int dato) {
        //caso base
        if(isEmpty()) {
            this.dato = dato;
            this.izq = new Tree();
            this.der = new Tree();
            return;
        }
        // otros casos
        if (this.dato > dato) {
            izq.add(dato);
        } else {
            der.add(dato);
        }
    }
    public boolean isEmpty() {
        return der == null && izq == null;
    }
    public String toString() {
        String strDer = der.isEmpty() ? "" : ", der->" + der;
        String strIzq = izq.isEmpty() ? "" : ", izq->" + izq;
        return "[" + dato + strDer +  strIzq + "]" ;
    }
}
