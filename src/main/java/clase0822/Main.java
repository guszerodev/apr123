package clase0822;

import clase0822.recursividad.Contador;
import clase0822.recursividad.Infinito;
import clase0822.recursividad.LlamadasEncadenadas;
import clase0822.recursividad.RecursividadvsIteracion;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion:.................
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion:.................
        //Contador.contarHasta();

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));

        for (int i = 0; i <= 12 ; i++) {
            System.out.printf("%d, ", RecursividadvsIteracion.fibonacciRecu(i));
        }
        System.out.println();
        for (int i = 0; i <= 12 ; i++) {
            System.out.printf("%d, ", RecursividadvsIteracion.fibonacciIter(i));
        }
    }
}
