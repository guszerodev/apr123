package clase0809.genericos;

import java.util.ArrayList;
import java.util.List;

class Procesar<T> {
    T dato;
    public T test() {
        return this.dato;
    }
}
public class Main {
    public static void main(String[] args) {
        Procesar<String> p = new Procesar<String>();
        p.dato = "1";

        Procesar<Integer> p1 = new Procesar<Integer>();
        p1.dato = new Integer(4);

        List<String> lista = new ArrayList<String>();

        System.out.println(p.test());
        System.out.println(p1.test());
    }
}
