package clase0801.matrices;

public class Main {
    public static void main(String[] args) {
        int [][] m1 = new int[3][];
        m1[0] = new int[0];
        m1[1] = new int[1];
        m1[2] = new int[2];
        System.out.println();
        int [][] m2 = {{1,2},{3,4}};
        printMatrix(m2);
        System.out.println();

    }
    public static void printMatrix(int[][] m) {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.printf("%d\t", m[i][j]);
            }
            System.out.println();
        }
    }
    /*
    Implementar un juego que use un mapa (10x10) de coordenadas X Y, donde un jugador será ubicado (en una casilla) en una posición aleatoria dentro el mapa, luego 5 obstáculos dentro el mapa en posiciones aleatorias, similar que el jugador intenta llegar a la esquina inferior derecha del mapa, un paso a la vez, pasando de su posición actual a la derecha, izquierda, arriba, abajo. Siempre y cuando no se encuentre con un obstáculo.
    * */

}
