package clase0916.maps;

public class JUMapBasic<K,V> {
    private Object[] buckets = new Object[Integer.MAX_VALUE / 2];
    int size;
    private int hashFunction(K key) {
        int hashCode = key.hashCode() < 0 ? key.hashCode() * -1 : key.hashCode();

        return hashCode;
    }
    public V put(K key, V value) {
        int index = hashFunction(key);
        if (buckets[index] == null) {
            buckets[index] = value;
        } else {
            buckets[index] = value;
        }

        size++;
        return value;
    }
    public V get(Object key){
        int index = hashFunction((K)key);
        if (buckets[index] != null) {
            return (V) buckets[index];
        } else {
            return null;
        }
    }

}
