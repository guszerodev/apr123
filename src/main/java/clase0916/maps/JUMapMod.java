package clase0916.maps;

public class JUMapMod<K,V> {
    int totalBuckets;
    private Object[] buckets;
    int size;

    public JUMapMod(int totalBuckets){
        this.totalBuckets = totalBuckets;
        buckets = new Object[totalBuckets];
    }

    private int hashFunction(K key) {
        int hashCode = key.hashCode() < 0 ? key.hashCode() * -1 : key.hashCode();

        hashCode = hashCode % totalBuckets;

        return hashCode;
    }
    public V put(K key, V value) {
        int index = hashFunction(key);
        buckets[index] = value;

        size++;
        return value;
    }
    public V get(Object key){
        int index = hashFunction((K)key);
        if (buckets[index] != null) {
            return (V) buckets[index];
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        String map = "";
        for(Object obj : buckets) {
            if (obj == null) {
                continue;
            }
            map += obj + ", ";
        }
        return map;
    }
}
