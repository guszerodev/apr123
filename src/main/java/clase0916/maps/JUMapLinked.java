package clase0916.maps;

class HashNode<K, V> {
    K key;
    V value;
    HashNode<K, V> next;
    public String toString() {
        String nextStr = next == null ? "" : ", " + next;
        return value + "" + nextStr;
    }
}

public class JUMapLinked<K,V> {
    int totalBuckets;
    private Object[] buckets;
    int size;

    public JUMapLinked(int totalBuckets){
        this.totalBuckets = totalBuckets;
        buckets = new Object[totalBuckets];
    }

    private int hashFunction(K key) {
        int hashCode = key.hashCode() < 0 ? key.hashCode() * -1 : key.hashCode();

        hashCode = hashCode % totalBuckets;

        return hashCode;
    }
    public V put(K key, V value) {
        int index = hashFunction(key);
        HashNode<K, V> node = (HashNode<K, V>)buckets[index];
        HashNode<K, V> prev = node;

        while (node != null) {
            if (node.key.equals(key) && node.key.hashCode() == key.hashCode()) {
                node.value = value;
                return value;
            }
            prev = node;
            node = node.next;
        }

        size++;
        HashNode<K, V> newNode = new HashNode<>();
        newNode.key = key;
        newNode.value = value;

        if (prev == null) {
            buckets[index] = newNode;
            return value;
        }
        prev.next = newNode;
        return value;
    }
    public V get(Object key){
        int index = hashFunction((K)key);
        HashNode<K, V> node = (HashNode<K, V>)buckets[index];
        HashNode<K, V> prev = node;

        while (node != null) {
            if (node.key.equals(key) && node.key.hashCode() == key.hashCode()) {
                return node.value;
            }
            node = node.next;
        }

        return null;
    }

    @Override
    public String toString() {
        String map = "";
        for(Object obj : buckets) {
            if (obj == null) {
                continue;
            }
            map += obj + ", ";
        }
        return map;
    }
}
