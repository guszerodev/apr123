package clase0916;

import clase0908.sets.Person;
import clase0916.maps.JUMapBasic;
import clase0916.maps.JUMapLinked;
import clase0916.maps.JUMapMod;

import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //testBasicStringInteger();
//        testBasicPersonString();

        String n1 = "John";
        String n2 = "Ahn";
        String n3 = "Chris";
        String n4 = "Steven";
        System.out.printf("%d, %d, %d, %d, \n", n1.hashCode(), n2.hashCode(), n3.hashCode(), n4.hashCode());
//        testBasicPersonString2();
//        testModPersonString(5);
        testLinkedPersonString(3);
    }

    private static void testBasicStringInteger() {
        JUMapBasic<String, Integer> map = new JUMapBasic<>();
        map.put("1", 1);
        map.put("2", 2);
        System.out.println(map.get("2"));
    }
    private static void testBasicPersonString() {
        JUMapBasic<Person, String> map = new JUMapBasic<>();
        List<Person> persons = Person.dummyData();
        for (Person person : persons) {
            map.put(person, person.getName());
        }
        Person chris = new Person("Chris", 19);
        System.out.println(map.get(chris));
    }
    private static void testBasicPersonString2() {
        JUMapBasic<Person, String> map = new JUMapBasic<>();
        map.put(new Person("Chris", 19), "Chris");
        map.put(new Person("Chris", 19), "Chris2");
        map.put(new Person("John", 20), "John");
        Person chris = new Person("Chris", 19);
        System.out.println(map.get(chris));
    }
    private static void testModPersonString(int totalBuckets) {
        JUMapMod<Person, String> map = new JUMapMod<>(totalBuckets);
        List<Person> persons = Person.dummyData();
        for (Person person : persons) {
            map.put(person, person.getName());
        }
        Person chris = new Person("Chris", 19);
        System.out.println(map.get(chris));
        System.out.println(map);
    }

    private static void testLinkedPersonString(int totalBuckets) {
        JUMapLinked<Person, String> map = new JUMapLinked<>(totalBuckets);
        List<Person> persons = Person.dummyData();
        for (Person person : persons) {
            map.put(person, person.getName());
        }
        Person chris = new Person("Chris", 19);
        System.out.println(map.get(chris));
        System.out.println(map);
    }
}
