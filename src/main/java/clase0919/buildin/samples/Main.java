package clase0919.buildin.samples;

import java.util.HashMap;
import java.util.Map;

class Student{
    int id;
    String name;
    Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        Student s = (Student)obj;
        return name.equals(s.name) && id == s.id;
    }
}
class Grade {
    int studentId;
    float grade;
    Grade(int id, float grade) {
        studentId = id;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "studentId=" + studentId +
                ", grade=" + grade +
                '}';
    }

}
public class Main {
    public static void main(String[] args) {
        Map<Student, Grade> grades = new HashMap<>();
        grades.put(new Student(1, "Chris"), new Grade(1, 100));
        grades.put(new Student(1, "Chris"), new Grade(1, 50));
        System.out.println(grades);
    }
}
