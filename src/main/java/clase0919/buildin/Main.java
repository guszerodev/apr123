package clase0919.buildin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
//        testHashTable();
        testHashMap();
    }

    private static void testHashTable() {
        System.out.println("testHashTable");
        Map<String, Integer> table = new Hashtable<>();
        table.put("Uno", 1);
        table.put("Dos", 2);
        table.put("Tres", 3);
        for (Map.Entry<String, Integer> entry : table.entrySet()) {
            System.out.println(entry.getKey() + "|" + entry.getValue());
        }
    }
    private static void testHashMap() {
        System.out.println("testHashMap");
        Map<String, Integer> map = new HashMap<>();
        map.put("Uno", 1);
        map.put("Dos", 2);
        map.put("Tres", 3);
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "|" + entry.getValue());
        }
    }
}
