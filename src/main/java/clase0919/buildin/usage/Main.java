package clase0919.buildin.usage;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
//        iterateMaps();
        updating();
    }

    private static void iterateMaps() {
        System.out.println("iterateMaps");
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Uno", 1);
        map.put("Dos", 2);
        map.put("Tres", 3);
        System.out.println("keys:");
        for (String key : map.keySet()) {
            System.out.print(key + ", ");
        }
        System.out.printf("\nValues:");
        for (Integer value : map.values()) {
            System.out.print(value + ", ");
        }

    }
    private static void updating() {
        System.out.println("updating");
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Uno", 1);
        map.put("Dos", 2);
        map.put("Tres", 3);
        System.out.println(map);
        map.put("Tres", 0);
        System.out.println(map);
        map.replace("Tres", 3);
        map.replace("Cuatro", 4);
        System.out.println(map);
        map.remove("Tres");
        System.out.println(map);

    }
}
