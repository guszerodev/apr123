package clase0919.maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

class HashNode<K, V> {
    K key;
    V value;
    HashNode<K, V> next;
    public String toString() {
        String nextStr = next == null ? "" : ", " + next;
        return value + "" + nextStr;
    }
}

public class JUHashMap<K,V> implements Map<K,V> {
    int totalBuckets;
    int size;
    private ArrayList<HashNode<K, V>> buckets;

    public JUHashMap(int totalBuckets){
        this.totalBuckets = totalBuckets;
        buckets = new ArrayList<>(totalBuckets);
        for (int i = 0; i < totalBuckets; i++)
            buckets.add(null);
    }

    private int hashFunction(K key) {
        int hashCode = key.hashCode() < 0 ? key.hashCode() * -1 : key.hashCode();

        hashCode = hashCode % totalBuckets;

        return hashCode;
    }
    public V put(K key, V value) {
//        rehashing();
        int index = hashFunction(key);
        HashNode<K, V> node = buckets.get(index);
        HashNode<K, V> prev = node;

        while (node != null) {
            if (node.key.equals(key) && node.key.hashCode() == key.hashCode()) {
                node.value = value;
                return value;
            }
            prev = node;
            node = node.next;
        }

        size++;
        HashNode<K, V> newNode = new HashNode<>();
        newNode.key = key;
        newNode.value = value;

        if (prev == null) {
            buckets.set(index, newNode);
            return value;
        }
        prev.next = newNode;
        return value;
    }

    private void rehashing() {
        totalBuckets *= 2;
        ArrayList<HashNode<K, V>> bucketsTemp = new ArrayList<>(totalBuckets);
        for (int i = 0; i < buckets.size() ; i++) {
            K key = buckets.get(i).key;
            V value = buckets.get(i).value;
            int index = hashFunction(key);
            HashNode<K, V> node = bucketsTemp.get(index);
            HashNode<K, V> prev = node;

            while (node != null) {
                if (node.key.equals(key) && node.key.hashCode() == key.hashCode()) {
                    node.value = value;
                    break;
                }
                prev = node;
                node = node.next;
            }

            HashNode<K, V> newNode = new HashNode<>();
            newNode.key = key;
            newNode.value = value;
            bucketsTemp.add(buckets.get(i));
        }

        buckets = bucketsTemp;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    public V get(Object key){
        int index = hashFunction((K)key);
        HashNode<K, V> node = buckets.get(index);
        HashNode<K, V> prev = node;

        while (node != null) {
            if (node.key.equals(key) && node.key.hashCode() == key.hashCode()) {
                return node.value;
            }
            node = node.next;
        }

        return null;
    }
    public String toString() {
        String map = "";
        for(Object obj : buckets) {
            if (obj == null) {
                continue;
            }
            map += obj + ", ";
        }
        return map;
    }
}
