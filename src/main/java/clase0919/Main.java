package clase0919;

import clase0908.sets.Person;
import clase0919.maps.JUHashMap;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        testPersonString(10);
    }

    private static void testPersonString(int totalBuckets) {
        Map<Person, String> map = new JUHashMap<>(totalBuckets);
        List<Person> persons = Person.dummyData();
        for (Person person : persons) {
            map.put(person, person.getName());
        }
        Person chris = new Person("Chris", 19);
        System.out.println(map.get(chris));
        System.out.println(map);
        Hashtable table;
    }
}
