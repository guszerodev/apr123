package clase0908.arrayvslinked;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

class NodeInt {
    int data;
    NodeInt next;
    public NodeInt(int data) { this.data = data;}
}
class JULinkedList {
    NodeInt root;
    public void add(int data) {
        NodeInt node = new NodeInt(data);
        node.next = root;
        root = node;
    }

}

public class Main {
    public static void main(String[] args) {
        int total = args.length > 0 ? Integer.parseInt(args[0]) : 50_000_000;
        Scanner in = new Scanner(System.in);
//        crearJULinked(total);
//        System.out.println("gona clean");
//        in.nextLine();
//        System.gc();
//        System.out.println("freed");
        crearLinked(total);
        System.out.println("gona clean");
        in.nextLine();
        System.gc();
        System.out.println("freed");
        crearArray(total);
        System.out.println("gona clean");
        in.nextLine();
        System.gc();
        System.out.println("freed");
        in.nextLine();
    }
    public static void crearArray(int total) {
        Scanner in = new Scanner(System.in);
        System.out.println("arrayList");
        in.nextLine();
        List<Integer> arrayList = new ArrayList<>();
        Instant inst1 = Instant.now();
        arrayList.add(10);arrayList.add(10);
        for (int i = 0; i < total; i++) {
            arrayList.add(i, (int) (Math.random() * total));
        }
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
        System.out.println("ref still alive");
        in.nextLine();
    }
    public static void crearLinked(int total) {
        Scanner in = new Scanner(System.in);
        System.out.println("linkedList");
        in.nextLine();
        List<Integer> linkedList = new LinkedList<>();
        Instant inst1 = Instant.now();
        linkedList.add(10);linkedList.add(10);
        for (int i = 0; i < total; i++) {
            linkedList.add(i, (int) (Math.random() * total));
        }
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
        System.out.println("ref still alive");
        in.nextLine();
    }
    public static void crearJULinked(int total) {
        Scanner in = new Scanner(System.in);
        System.out.println("JULinked");
        in.nextLine();
        JULinkedList linkedList = new JULinkedList();
        Instant inst1 = Instant.now();
        for (int i = 0; i < total; i++) {
            linkedList.add((int) (Math.random() * total));
        }
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
        System.out.println("ref still alive");
        in.nextLine();
    }
}
