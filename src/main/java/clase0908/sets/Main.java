package clase0908.sets;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

class SetList<T extends Comparable<T>> extends ArrayList<T>{
    @Override
    public boolean add(T t) {
        if (!contains(t)) {
            return super.add(t);
        }
        return false;
    }

    public boolean addSort(T t) {
        for (int i = 0; i < size(); i++) {
            if (t.compareTo(get(i)) < 0){
                add(i, t);
                return true;
            } else if(t.compareTo(get(i)) == 0){
                return false;
            }
        }
        add(size(), t);
        return true;
    }
}


public class Main {
    public static void main(String[] args) {
        int total = args.length > 0 ? Integer.parseInt(args[0]) : 1_000;
        Scanner in = new Scanner(System.in);
        int[] dummyDataArray = CreateDummyData(total);

        System.out.println("starting...");
        //in.nextLine();

        testBasicSet(dummyDataArray);
        testBasicSetAddSort(dummyDataArray);
//        testTreeSet(dummyDataArray);
//        System.out.println("gona free");
////        in.nextLine();
//        System.gc();
//        testLinkedSet(dummyDataArray);
//        System.out.println("gona free");
////        in.nextLine();
//        System.gc();
//        testHashSet(dummyDataArray);
//        System.out.println("gona free");
//        //in.nextLine();
//        System.gc();
        System.out.println("freed");
    }

    private static void testBasicSet(int[] dummyDataArray) {
        System.out.println("testBasicSet");
        SetList<Integer> list = new SetList<>();
        Instant inst1 = Instant.now();
        for(int data : dummyDataArray){
            list.add(data);
        }
        System.out.println(list.size());
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
    }

    private static void testBasicSetAddSort(int[] dummyDataArray) {
        System.out.println("testBasicSetAddSort");
        SetList<Integer> list = new SetList<>();
        Instant inst1 = Instant.now();
        for(int data : dummyDataArray){
            list.addSort(data);
        }
        System.out.println(list.size());
        System.out.println(list);
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
    }
    private static void testTreeSet(int[] dummyDataArray) {
        System.out.println("testTreeSet");
        Set<Integer> list = new TreeSet<>();
        Instant inst1 = Instant.now();
        for(int data : dummyDataArray){
            list.add(data);
        }
        System.out.println(list.size());
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
    }
    private static void testLinkedSet(int[] dummyDataArray) {
        System.out.println("testLinkedSet");
        Set<Integer> list = new LinkedHashSet<>();
        Instant inst1 = Instant.now();
        for(int data : dummyDataArray){
            list.add(data);
        }
        System.out.println(list.size());
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
    }
    private static void testHashSet(int[] dummyDataArray) {
        System.out.println("testHashSet");
        Set<Integer> list = new HashSet<>();
        Instant inst1 = Instant.now();
        for(int data : dummyDataArray){
            list.add(data);
        }
        System.out.println(list.size());
        Instant inst2 = Instant.now();
        System.out.println("Elapsed Time: "+ Duration.between(inst1, inst2).toMillis());
    }

    private static int[] CreateDummyData(int total) {
        int[] nums = new int[total];
        for (int i = 0; i < total; i++) {
            nums[i] = (int) (Math.random() * total);
        }

        return nums;
    }
}
