package clase0908.sets;

import java.util.List;

public class Person extends Object {
    private String name;
    private int age;
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "{" + name + "," + age + "}";
    }

    // sets needs to override `equals` and `hashCode`
    // if `equals` is true then `hashCode`s should be the same
    // `hashCode`s could be the same, but `equals` could be false
    // if Override `equals` then also `hashCode`
    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj))
            return true;

        if (!(obj instanceof Person))
            return false;

        Person p = (Person) obj;

        return name.equals(p.name) && age == p.age;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + age;
    }

    public static List<Person> dummyData(){
        return List.of(new Person("John", 20),
                new Person("Ahn", 22),
                new Person("Chris", 19),
                new Person("Steven", 25));
    }
}
