package clase0908.sets.samples.tree;

import clase0908.sets.Person;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> strings = new TreeSet<>(List.of("a", "c", "b"));
        strings.add("c");
        System.out.println(strings);

        Comparator<Person> byName = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        };
        Set<Person> persons = new TreeSet<>(byName);
        persons.addAll(Person.dummyData());
//        persons.add(new Person("Chris", 19));
        persons.add(new Person("chris", 19));
        System.out.println(persons);
    }
}
