package clase0908.sets.samples.hash;

import clase0908.sets.Person;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> strings = new HashSet<>(List.of("a", "c", "b"));
        strings.add("c");
        strings.add("d");
        System.out.println(strings);

        Set<Person> persons = new HashSet<>(Person.dummyData());
        persons.add(new Person("Chris", 19));
        System.out.println(persons);
    }
}
