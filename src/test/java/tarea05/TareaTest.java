package tarea05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TareaTest {
    @Test
    void canCreateAppMain(){
        Tarea t = new Tarea();
        assertNotNull(t, "App Main should be created");
    }
}
