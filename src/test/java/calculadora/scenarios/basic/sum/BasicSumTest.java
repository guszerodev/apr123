package calculadora.scenarios.basic.sum;

import calculadora.AbstractCalc;
import calculadora.MockCalc;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicSumTest {
    @Test
    public void twoOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("1+1");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void severalOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void largeIntsOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("2000000+1");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void validSpacesOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("  1+1");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void moreValidSpacesOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc(" 1  +  1 ");
        int expected = 1;

        assertEquals(expected,actual);
    }
    @Test
    public void severalOperantsAndSpacesSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("  1  + 1   + 1+1  +1+1  +1+ 1+1+  1+1+1+1+1  +1 +1 +1 +1 +1 +1 +1 +1+1+1+1+1+1+1+1+1+1  +1   ");
        int expected = 1;

        assertEquals(expected,actual);
    }
}
