package calculadora.scenarios.bracket.sum;

import calculadora.AbstractCalc;
import calculadora.MockCalc;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumOperationUsingBracketsTest {
    @Test
    public void twoOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("(1+1)");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void moreBracketsOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("(1+1) + (1 + 1)");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void nestedBracketsOperantsSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = calc.calc("(1+1) + (1 + (1 + (1+1)))");
        int expected = 1;

        assertEquals(expected,actual);
    }
}
