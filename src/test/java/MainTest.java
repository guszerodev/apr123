import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MainTest {
    @Test
    void canCreateAppMain(){
        Main m1 = new Main();
        assertNotNull(m1, "App Main should be created");
    }
}
